package com.qjl;

import cn.hutool.core.io.resource.ResourceUtil;
import com.qjl.core.ELNode;
import com.qjl.core.ExpressLanguageParseException;
import com.qjl.parser.ELParser;
import com.qjl.parser.ELTransfer;
import com.qjl.parser.logicflow.*;
import com.qjl.util.Json2ELNodeParser;
import org.junit.Test;

import java.util.*;

/**
 * @author : zhangrongyan
 * @date : 2023/1/13 14:02
 */
public class TestParser {
	public static void doWithElNodeJson(String path) throws ExpressLanguageParseException {
		String json = ResourceUtil.readUtf8Str(path);
		Json2ELNodeParser json2ELNodeParser = new Json2ELNodeParser(json);
		ELNode el = json2ELNodeParser.parse();
		String elStr = el.generateEl();
		System.out.println(elStr);
	}

	public static void doWithLogicFlowJson(String path) {
		String json = ResourceUtil.readUtf8Str(path);
		Json2ELNodeParser json2ELNodeParser = new Json2ELNodeParser(json);
		LogicFlow logicFlow = json2ELNodeParser.parseLogicFlow();
		ELParser parser = new LogicFlowParser(logicFlow);
		ELNode elNodeFromLogicFlowData = parser.extractElNode();
		try {
			System.out.println(elNodeFromLogicFlowData.generateEl());
		} catch (ExpressLanguageParseException e) {
			e.printStackTrace();
		}
	}


	public static void transWithLogicFlowJson(String path) {
		String json = ResourceUtil.readUtf8Str(path);
		Json2ELNodeParser json2ELNodeParser = new Json2ELNodeParser(json);
		LogicFlow logicFlow = json2ELNodeParser.parseLogicFlow();

		LogicFlowManager logicFlowManager = new LogicFlowManager(logicFlow);

		LfParseContext lfParseContext = new LfParseContext();

		ELTransfer transer = new LogicFLowTransfer(logicFlowManager, lfParseContext);

		try {
			TransferNode trans = transer.transfer();
//			System.out.println(trans);
			TransferParser transferParser = new TransferParser(trans);
			ELNode elNode = transferParser.extractElNode();
			System.out.println(elNode.generateEl());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Test
	public void testTransferSimpleThenLfData() throws Exception {
		transWithLogicFlowJson("json/example/logicflow_data/simpleThen.json");

	}

	@Test
	public void testTransferSimpleSwitchLfData() throws Exception {
		transWithLogicFlowJson("json/example/logicflow_data/simpleSwitch.json");

	}

	@Test
	public void testTransferSimpleWhenLfData() throws Exception {
		transWithLogicFlowJson("json/example/logicflow_data/simpleWhen.json");

	}


	@Test
	public void testTransferWhenInWhenLfData() throws Exception {
		transWithLogicFlowJson("json/example/logicflow_data/whenInWhen.json");

	}

	@Test
	public void testTransferComplexWhenInWhenLfData() throws Exception {
		transWithLogicFlowJson("json/example/logicflow_data/complexWhen.json");

	}

	@Test
	public void testLogicFlowDataIfInWhen() throws ExpressLanguageParseException {
		transWithLogicFlowJson("json/example/logicflow_data/ifInWhen.json");
	}

	/**
	 * 手动构造TransNode 测试When
	 * @throws Exception
	 */
	@Test
	public void testTransferSimpleWhen() throws Exception {

		TransferNode transferNode = buildTransNode(ELNode.ELNameEnum.COMMON.name(), "id1");

		TransferNode transferNode1 = buildTransNode(ELNode.ELNameEnum.WHEN.name(), "id2");
		transferNode.setNext(Collections.singletonList(transferNode1));
		transferNode1.setChildren(Arrays.asList(
				buildTransNode(ELNode.ELNameEnum.COMMON.name(), "id4"),
				buildTransNode(ELNode.ELNameEnum.COMMON.name(), "id5")));


		TransferNode transferNode2 = buildTransNode(ELNode.ELNameEnum.COMMON.name(), "id3");
		transferNode1.setNext(Collections.singletonList(transferNode2));


		System.out.println(transferNode);
		TransferParser transferParser = new TransferParser(transferNode);
		ELNode elNode = transferParser.extractElNode();
		System.out.println(elNode);
		System.out.println("success");
		System.out.println(elNode.generateEl());

	}

	private Map<String, Object> getMap(String nodeType, String nodeId) {
		Map<String, Object> map = new HashMap<>();
		map.put("nodeType", nodeType);
		map.put("nodeId", nodeId);
		return map;
	}

	private TransferNode buildTransNode(String nodeType, String nodeId) {
		TransferNode transferNode = new TransferNode();
		LfNode lfNode = new LfNode();
		lfNode.setProperties(getMap(nodeType, nodeId));
		transferNode.setLfNode(lfNode);
		return transferNode;
	}


	@Test
	public void testSimpleIf2() {
		transWithLogicFlowJson("json/example/logicflow_data/simpleIf.json");
		transWithLogicFlowJson("json/example/logicflow_data/simpleIf2.json");
	}

	@Test
	public void testSimpleSwitch() {
		transWithLogicFlowJson("json/example/logicflow_data/logicFlowSwitchData.json");
	}



	@Test
	public void testPublicActivity() throws ExpressLanguageParseException {
		doWithElNodeJson("json/example/elnode_data/publishActivity2.json");

	}

	@Test
	public void testSimpleWhen() throws ExpressLanguageParseException {
		doWithElNodeJson("json/example/elnode_data/simpleWhen.json");

	}





}
