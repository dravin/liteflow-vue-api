package com.qjl.parser;

import com.qjl.parser.logicflow.TransferNode;

public interface ELTransfer {

	TransferNode transfer();

}
