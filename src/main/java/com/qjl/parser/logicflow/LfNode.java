package com.qjl.parser.logicflow;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author : zhangrongyan
 * @date : 2023/3/3 16:29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LfNode {
    private String id;
    private String type;
    private Integer x;
    private Integer y;
    private Map<String,Object> properties;
    private TextEntity text;
    private List<String> children;

    @Data
    public static class TextEntity {
        private Integer x;
        private Integer y;
        private String value;
    }

	/**
	 * 获取Lf节点属性
	 *
	 * @param propertyName 属性名字
	 * @return {@link String}
	 */
	public String getLfNodeProperty( String propertyName) {
		return Optional.of(this)
				.map(LfNode::getProperties)
				.map(e -> e.get(propertyName))
				.map(Object::toString)
				.orElse("");
	}
}
