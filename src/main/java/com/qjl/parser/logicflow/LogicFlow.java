package com.qjl.parser.logicflow;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author : zhangrongyan
 * @date : 2023/3/3 16:40
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogicFlow {
	List<LfNode> nodes;

	List<LfEdge> edges;


}
