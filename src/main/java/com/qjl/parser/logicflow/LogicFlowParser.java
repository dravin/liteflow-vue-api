package com.qjl.parser.logicflow;

import com.qjl.core.ELNode;
import com.qjl.parser.ELParser;
import com.qjl.parser.ELTransfer;
import lombok.Data;

import java.util.Objects;

/**
 * @author : zhangrongyan
 * @date : 2023/3/3 16:51
 */
@Data
public class LogicFlowParser implements ELParser {

	ELTransfer elTransfer;

	/**
	 * 构造函数
	 *
	 * @param logicFlow
	 */
	public LogicFlowParser(LogicFlow logicFlow) {

		LogicFlowManager logicFlowManager = new LogicFlowManager(logicFlow);
		LfParseContext lfParseContext = new LfParseContext();

		this.elTransfer = new LogicFLowTransfer(logicFlowManager, lfParseContext);
	}

	/**
	 * 解析ELNode的实现
	 *
	 * @return
	 */
	@Override
	public ELNode extractElNode() {
		TransferNode transferNode = this.elTransfer.transfer();
		if (Objects.isNull(transferNode)) {
			return null;
		}
		TransferParser transferParser = new TransferParser(transferNode);

		return transferParser.extractElNode();
	}

}
