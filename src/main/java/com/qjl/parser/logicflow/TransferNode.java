package com.qjl.parser.logicflow;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferNode {
	private LfNode lfNode;
	private List<TransferNode> children = new ArrayList<>();

	/**
	 * common/if/switch/抽象when组
	 */
	private List<TransferNode> next=new ArrayList<>();

	/**
	 * 添加下一个next节点
	 * @param transferNode
	 */
	public void addNext(TransferNode transferNode){
		this.next.add(transferNode);
	}

	/**
	 * 添加内子节点
	 * @param transferNode
	 */
	public void addChild(TransferNode transferNode){
		this.children.add(transferNode);
	}

}
