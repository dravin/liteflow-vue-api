package com.qjl.parser;

import com.qjl.core.ELNode;

/**
 *
 * @author : zhangrongyan
 * @date : 2023/3/9 11:03
 */
public interface ELParser {
    /**
     * 解析提取ELNode
     *
     * @return ELNode
     */
    ELNode extractElNode();
}
