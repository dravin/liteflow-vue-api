package com.qjl.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qjl.core.ELNode;
import com.qjl.parser.logicflow.LogicFlow;

import java.io.IOException;

/**
 * @author : zhangrongyan
 * @date : 2023/1/13 12:24
 */
public class Json2ELNodeParser {
    ObjectMapper objectMapper;
    private String jsonStr;

    public Json2ELNodeParser(String jsonStr) {
        this.jsonStr = jsonStr;
        this.objectMapper = new ObjectMapper();
    }

    public ELNode parse() {
        try {
            return objectMapper.readValue(this.jsonStr, ELNode.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LogicFlow parseLogicFlow() {
        try {
            return objectMapper.readValue(this.jsonStr, LogicFlow.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
