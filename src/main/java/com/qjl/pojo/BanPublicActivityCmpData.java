package com.qjl.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : zhangrongyan
 * @date : 2023/1/12 18:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BanPublicActivityCmpData {
    private Integer days;
}
