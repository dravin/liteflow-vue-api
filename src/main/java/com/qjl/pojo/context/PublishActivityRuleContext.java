package com.qjl.pojo.context;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 发布活动风控规则的上下文
 * @author : zhangrongyan
 * @date : 2023/1/12 18:43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublishActivityRuleContext {
    private Integer creditScore;
    private Integer ghomeDays;
    private Boolean starGroup;
}

