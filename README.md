# Liteflow-vue-api 的后端项目

前端代码在这里 [liteflow-logicflow-vue](https://gitee.com/dravin/liteflow-logicflow-vue)

## 接收LogicFlow的数据结构
- 接收logicFlow前端的json，转化el表达式,演示接口：/api/generateLogicFlowEL

- 效果：
  ![](doc/lfimg.png)

## 接收ELNode 数据结构
- 接收前端的json，转化el表达式,演示接口：/api/generateEL

- 效果：
![](doc/img.jpg)


![skimg](doc/sk.jpg)


## 感谢

| 壕气老板  | 赞助时间        |
|-------|-------------|
| 😊    | 2024年8月15日  |
| 老欧阳啊  | 2024年5月24日  |
| 飞飞    | 2023年10月24日 |
| 草帽    | 2023年8月10日  |
| 疯狂挖掘机 | 2023年4月12日  |
| K.    | 2023年4月12日  |
| 源     | 2023年3月20日  |
| jjj   | 2023年3月20日  |
